Gem::Specification.new do |s|
	s.name	=	"common"
	s.version	=	"2.0.0"
	s.authors	=	["Art Griesser", 'Michael Faughn']
	s.email	=	["a.griesser@prometheuscomputing.com", 'm.faughn@prometheuscomputing.com']
	s.summary	=	"Functionality shared by several projects"
	s.require_paths	=	['lib']
	s.files	=	["README.txt", "lib/common.rb", "lib/common/constants.rb", "lib/common/log.rb", "lib/common/meta_info.rb", "lib/common/multi_io.rb", "lib/common/module_extensions.rb", "lib/common/object_logger.rb", "lib/common/ruby_extensions.rb", "lib/common/send_chained.rb", "spec/chained_method_spec.rb", "spec/multi_io_spec.rb", "spec/object_logger_spec.rb", "spec/spec_helper.rb", "test/test_log.rb"]
end
