# projects known to use this include: plugin.modelGen, umm_ruby_gen, magicdraw_extensions, json_graph, gui_director, gui_site, sequel_specific_associations, appellation
class String
  # Evaluate the string as a constant
  # Note: returns given namespace (typically Object) if given empty string or a string only containing '.'s and/or '::'s
  # Known issue: returns Object if given empty string or a string only containing '.'s and/or '::'s
  # NOTE: this does not act the same as Object#const_get, since it fully traverses the namespace tree,
  #  when evaluating constants, whereas #const_get only looks at the current namespace and at Object's namespace.
  def to_const(namespace = Object)
    return Object if self.gsub(/\.|::/, '').empty?
    # puts "#{self.inspect}.to_const(#{namespace.inspect})"
    ns = namespace
    found = false
    while !found
      # puts "  namespace is now #{namespace} -- self is #{self.inspect}"
      found = ns.const_defined?(self)
      if found
        return ns.const_get(self)
      else
        raise NameError.new("Could not find constant #{self} in namespace: #{namespace}") if ns == Object
        ns = ns.namespace
      end
    end
  end
end

# Add shortcut methods to symbol
class Symbol
  def to_const(namespace = Object)
    to_s.to_const(namespace)
  end
end

class Module
  def to_const; self; end
  
  # Get the containing namespace constant for this module
  # Returns Object if there is no named containing namespace
   # There used to be a #container method that did almost the same thing that #namespace does.  #container was a poor name due to collisions.
  def namespace
    # reverse.split('::', 2).last&.reverse&.to_const || Object # silly but works
    name.slice(/.+(?=::[A-Za-z]+)/)&.to_const || Object
    #
    # namespace_name = self.name.to_s.split("::")
    # namespace_name.pop # Remove class from namespace_name
    # return Object if namespace_name.empty?
    # namespace = namespace_name.join('::').to_const
  end
  
  def classes(options = {})
    ConstantsHelper.classes(self, options)
  end
  
  def owned_classes
    classes(:no_imports => true)
  end
  
  def modules(options = {})
    ConstantsHelper.modules(self, options)
  end
  
  def directly_contained_classes
    Kernel.warn("Module#directly_contained_classes is deprecated. There are corner cases that result in unexpected behavior.  Find an alternative! (look at the method source code...)")
    return [] if self.is_a?(Class) # <-- this means inner classes can't be returned
    constants.collect{|const| const_get(const)}.select{|const| const.is_a?(Class) && const.to_s =~ Regexp.new("^#{self.name}::[A-Za-z_]+$")} # <-- This regex fails for inner classes
  end
  
  def directly_contained_classes_fixed
    constants.map { |const| const_get(const) }.select { |const| Class === const && direct_const?(const) }
  end

  # adapted from http://stackoverflow.com/questions/9848153/how-do-you-find-all-modules-and-classes-within-a-module-recursively
  def contained_modules
    # self can't possibly contain self...so this is wrong.
    Kernel.warn("Module#contained_modules returns the receiver in the collection of contained modules.  This is clearly wrong, so this method is deprecated.  Find an alternative! (Hint: call Module#modules and then add the receiver to the collection manually)")
    [self] + constants.collect {|const| const_get(const) }.select {|const| const.is_a?(Module) && !(const.is_a?(Class))}.flat_map {|const| const.contained_modules }
  end
  
  def direct_const?(const)
    const.name =~ /^#{name}::[A-Za-z_]+/
  end

end

module ConstantsHelper
  module_function
  
  # while Classes are Modules, this method is intended to return only instances of Module (and not instances of Class)
  # FIXME do we want to include modules from includes here?
  def modules(mod, options = {})
    raise "Cannot determine constant from #{mod.inspect}" unless mod.respond_to?(:to_const)
    mod = mod.to_const
    return nil if mod.is_a?(Class) # or should it raise?
    sub_mods = mod.constants.map { |c| c.to_const(mod) }.select { |const| Module === const && !(Class === const) }
    sub_mods << sub_mods.map{ |sub_mod| modules(sub_mod, options) }
    sub_mods.flatten.uniq
  end
  
  def classes(mod, options = {})    
    raise "Cannot determine constant from #{mod.inspect}" unless mod.respond_to?(:to_const)
    mod = mod.to_const
    case mod
    when Class
      classes_of_class(mod, options)
    when Module
      classes_of_module(mod, options)
    else
      raise "Can't call #class on #{mod.inspect}"
    end
  end
  
  # The no imports bit will exclude mixed-in classes and will also exclude constants that point to a class that is defined in a different namespace.
  def classes_of_module(mod, options = {})
    recurse = !options[:direct]
    mod_classes  = mod.constants.map { |sym| sym.to_const(mod) }.select { |const| Class === const }
    if recurse
      sub_mods = modules(mod, options)
      mod_classes << sub_mods.map { |sm| classes_of_module(sm, options.merge(direct:true)) }
      mod_classes = mod_classes.flatten.select { |c| mod.direct_const?(c) } if options[:no_imports]
    end
    class_classes = mod_classes.flatten.map { |c| classes_of_class(c, options) }
    (mod_classes + class_classes).flatten.uniq.select { |c| c.name =~ /#{options[:pattern].to_s}/ }
  end
  
  # The no imports bit will exclude mixed-in classes and will also exclude constants that point to a class that is defined in a different namespace.
  def classes_of_class(klass, options = {})
    classes = klass.constants.map { |sym| sym.to_const(klass) }.select { |const| Class === const }
    classes = classes.select { |c| klass.direct_const?(c) } if options[:no_imports]
    class_classes = classes.map { |c| classes_of_class(c, options) }
    (classes + class_classes).flatten.uniq.select { |c| c.name =~ /#{options[:pattern].to_s}/ }
  end
end