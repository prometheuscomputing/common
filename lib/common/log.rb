require 'logger'
require 'common/constants'

#  =begin
#  Simple logging.  Lets you control logging at the Class or Module level.
#  This functionality, based on NaHi's
#  Dev-Logger is similar to earlier functionality based on
#  Log4r.  Log4r has not been updated in several years, and also
#  seems to be a bit eccentric. In contrast, NaHi's
#  logger behaves consistently and is now part of the standard distribution.
#  
#  As before,
#  * All classes have :debug, :info, :deprecated, :warn, :error, and :fatal methods.
#    These methods are provided on both the instance and class sides.
#  * Unlike the Logger and Log4r :fatal methods, the instance/class :fatal methods provided here raise an
#    exception. It's a convenience: they are fatal, right?
#  * The instance/class :deprecated message can take the same arguments as the other log messages,
#    but it's easier to give it no arguments, and let it compute the name
#    of the deprecated method.
#    
#  Main differences from previous code:
#  * This aliases the DEPRECATED level to INFO. The deprecated
#    messages are supported, but handled by info level.
#  * Logger::ArrayDev replaces Log4r::ObjectOutputter
#  * By default this provides a single Logger, associated 
#    with class Object, rather than a Logger per class.
#    You can add Loggers to specific Modules and Classes,
#    but if you don't the Object Logger will be used.
#    Loggers can be chained as before, but now you
#    need to do so explicitly, by using the parent
#    Logger as a LogDevice.
#  * The #outputters message resolves to the root Logger
#    (on Object, unless you create a chain that ends
#    elsewhere). It automatically converts the existing
#    LogDevice to a MuxDev.
#  * Methods such as log_error? have been added to both
#    class and instance sides. This is not consistent with
#    the use of error? on the Logger itself, but it a
#    avoids conflicts with (for example) error tests 
#    on objects that represent query results.
#  * Convenience methods (debug!, info!, warn!, error!, and fatal!)
#    have been added to set the logging level.
#    
#  Main differences between NaHi's Loggers and Log4r:
#  * NaHi's Loggers do not have names
#  * NaHi's Loggers take a LogDevice on instantiation.
#  * NaHi's Loggers record only Strings.
#    Log4r records LogEvent objects.
#    The Log4r approach is preferable for some purposes.
#    As a work-around, this file adds some messages to String,
#    so that the Strings look more like LogEvents.
#    
#  Suggested sematics for logging levels:
#  * debug   Information useful for heavy-duty debugging.
#            The results are thought to be fine.
#            Should NOT be routinely checked in production code.
#  * info    Information useful for getting oriented with the code, but useless for normal operation.
#            The results are thought to be fine.
#            Should NOT be routinely checked in production code.
#  * deprecated Information necessary to maitaining quality code.
#            The results are thought to be fine.
#            Should NOT be routinely checked in production code.
#  * warn    The user should be advised of something (such as an unimplemented feature).
#            The results may or may not be meaningful: this usually can not be programatically determined, the user must decide.
#            Should be routinely checked in production code.
#  * error   We encountered an error severe enough to render final results meaningless, although we can continue for the time being.
#            This allows us to accumulate multiple errors, so they can all be fixed in one swell foop.
#            Should be routinely checked in production code.
#  * fatal  The error is so severe that we can't proceed any further: 
#            Objects and their classes raise an exception immediately after logging
#            (although the logger itself does not). Should be routinely checked in production code.
#  * sanity_check  The level is the same as fatal, but should not be routinely checked in production code.
#                  The problem is fatal, and you can't proceed, but extensive testing has verified this will
#                  not be enounctered in production, so the performance penalty is undesirable.
#                  Should not be used for case statement "else" clauses that trap unexpected cases
#                  (because there is no performance penalty here): use fatal instead.
#  
#  A typical usage scenario would be:
#  
#      require 'common/log'
#     # Set the log level (actually on Object's logger, unless you specify another one):
#     SomeClass.logger.debug! # Alternative: SomeClass.logger.level= Logger::DEBUG
#     # Let's log something at the WARN level:
#     instanceOf_SomeClass.warn('The sky is falling') if some_cheap_test
#     # A block will let you bypass tests and message construction if the level is turned off:
#     instanceOf_SomeClass.warn {'The sky is falling' if some_expensive_test}
#  
#  Extra things you can do:
#     * Use the logger from the superclass:          SomeClass.use_superclass_logger
#     * Use the logger from the surrounding module:  SomeClass.use_container_logger
#     * Use a new logger:                            SomeClass.logger=Logger.new(STDOUT)
#     * Output to an array:                          SomeClass.logger.logdev= Logger::ArrayDev.new
#     * Several outputs at once:
#           dev = Logger::Mux.new
#           SomeClass.logger.logdev= dev << STDOUT << Logger::ArrayDev.new
#       or:
#           SomeClass.logger.outputters << Logger::ArrayDev.new # Presuming STDOUT was the original logdev.
#           
#  Status:
#  Chained loggers, use_superclass_logger, use_container_logger need to be better tested.
#  
#   
#  Author: A. Griesser
#  
#  =end

# Attempts to make a log entry look like a Log4r LogEvent.
class String
  def is_error
    char = self[0]
    ?E == char || ?F == char
  end
  def message
    sep = '-- : '
    i=self.index(sep)
    self[i+sep.length..-2]
  end
  def level; [?D, ?I, ?W, ?E, ?F].index(self[0]); end
  def level_name; ['DEBUG', 'INFO', 'WARN', 'ERROR', 'FAILURE'][level]; end
  alias severity level_name
  def trace; []; end
  alias backtrace trace
end


class LoggedFatal < RuntimeError
end


class Logger
  # If RAISE_FATAL_EXCEPTIONS is true, an exception will be raised after logging FATAL entries.
  # This occurs even if the logging of FATAL exceptions is deactivated.
  # This should probably always be true... with the possible exception of when testing the framework.
  RAISE_FATAL_EXCEPTIONS=true
  PERFORM_SANITY_CHECKS=true
  DEPRECATED=WARN
  DEFAULT_LEVEL=DEPRECATED
  LogMethods = [:debug, :info, :deprecated, :warn, :error, :fatal]
  
  alias original_initialize initialize
  def initialize(logdev, shift_age = 0, shift_size = 1048576)
    original_initialize(logdev, shift_age, shift_size)
    self.level=DEFAULT_LEVEL
    # Supress the timestamps. They are useful primarily when
    # logging to a file, and make debug output harder to read.
    self.datetime_format = ""
  end
  
  def _level_switch(new_level, &proc)
    old_level = level
    self.level=new_level
    return unless proc
    proc.call
    self.level=old_level
  end

  {:debug! => DEBUG, :info!  => INFO, :warn!  => WARN, :error! => ERROR, :fatal! => WARN}.each do |meth, const|
    define_method(meth) do
      if block_given?
        _level_switch(const, &Proc.new)
      else
        _level_switch(const)
      end
    end
  end
  #
  # def debug!(&proc); _level_switch(DEBUG, &proc); end
  # def info!(&proc);  _level_switch(INFO, &proc);  end
  # def warn!(&proc);  _level_switch(WARN, &proc);  end
  # def error!(&proc); _level_switch(ERROR, &proc); end
  # def fatal!(&proc); _level_switch(WARN, &proc);  end
  
  # Apparently in Ruby 1.9.2p290 the result of block evaluation is not checked for nil, so you can get messages like
  #     W, [#34088]  WARN -- : nil
  # if you code things like 
  #     warn {"Explanation" if condition? }
  # and condition? returns false.
  # Maybe this will be fixed, currently using in a few places
  #     warn("Explanation") if condition?
  
  
  # Lets you use a Logger as a LogDevice, so that 
  # Loggers can be chained. Lets you specify
  # a logfile in a single place. The loging level
  # formatter are determined by the first logger
  # in the chain, not the one at the end that 
  # actually writes to the IO/file/etc. 
  def write(message); @logdev.write(message); end
  
  # Lets you change the output device.
  attr_accessor :logdev
  
  # See comment on Module.logger=
  def logger; self; end
  
  def deprecated?; warn?; end
  def deprecated(data=nil)
    if block_given?
      warn("#{data} DEPRECATED", &Proc.new)
    else
      warn("#{data} DEPRECATED")
    end
  end
    
  # Returns a string that gets logged.
  def build_message(severity, progname, message)
    # looks like undocumented method format_severity takes an integer argument
    format_message(severity, Time.now, progname, message)
  end
  
  # Lets a logger output to several logdevs (such as a file and an IO).
  class MuxDev < Array
      # Arguments can be child LogDevices, or the symbol :ArrayDev,
      # which results in the creation of a new ArrayDev.
      def initialize(*logdevs)
        super()
        logdevs.each {|dev| self << :ArrayDev==dev ? ArrayDev.new : dev }
      end
      def write(message); self.each {|sub_logdev| sub_logdev.write(message)}; end
      def close; self.each {|sub_logdev| sub_logdev.write(message)}; end
      def history; self; end
  end
    
  # Gathers messages for programmatic evaluation. 
  # Similar to Log4r::ObjectOutputter found in the Log4r version.
  class ArrayDev < Array 
    def initialize(log=nil, opt={})
      super()
    end
    def write(message); self << message; end
    def close; end
    def history; self; end
  end # ArrayDev
  
  # An attempt at backward compatability
  class ObjectOutputter < ArrayDev
  end
  
  # Returns the ArrayDev
  def use_arrayDev
    self.logdev= ArrayDev.new
  end
  
  # The arguments can be used to specify sub logdevs.
  # If :ArrayDev is used as an argument, it is
  # converted into an ArrayDev.  STDOUT can be 
  # used as an argument. Returns the MuxDev
  def use_muxDev(*kids)
    self.logdev= MuxDev.new(*kids)
  end
  
  # Returns a MuxDev (converting the root LogDev
  # to a MuxDev if necessary). If the conversion
  # occurs, the old logdev becomes a child of the MuxDev.
  def outputters
    case logdev
    when MuxDev
      return logdev
    when Logger
      return logdev.outputter
    else
      self.logdev=MuxDev.new(logdev)
    end
  end
  
end

class NilClass
  # Eliminates some condtional logic from Module#logger
  def logger; Object._logger; end
end

class Module
  
  # ##############################################
  #   Logger management
  # ##############################################

  # You can actually specify a class or module as an
  # argument, in which case the logger will be resolved
  # when the logging event is handled. This lets you
  # swap out loggers without breaking the chain.
  def logger=(resolvable_to_logger)
    raise "Logger must be a Logger or a Module holding a Logger" unless resolvable_to_logger.kind_of?(Module) || resolvable_to_logger.kind_of?(Logger)
    @logger=resolvable_to_logger
  end
  def logger
    # The ||= nil supresses warnings aboug lack of initialization of @logger.
    (@logger||=nil).logger
  end

  def use_superclass_logger
    raise "Class/Module #{name} does not have a superlcass. " unless superclass
    self.logger=superclass
  end

  # FIXME this is broken.  There is no method #container
  # def use_container_logger
  #   outer = container
  #   raise "Class/Module #{name} does not have a parent Module. " unless outer
  #   self.logger=outer
  # end
  
  # This implementation of container is flawed. Classes nested more than one module deep will not resolve their container or will
  # resolve their container incorrectly. A better definition is now located in common/constants and required for use here. -SD
  # def container
  #   # I thought this method could just be 
  #   #    Module.nesting[1]
  #   # but that does not behave as I expected.
  #   # See container.rb experiment.
  #   container_name = self.name.split('::')[-2]
  #   return nil unless container_name
  #   eval(container_name)
  # end
  
end


class Object
  
  self.logger = Logger.new(STDOUT)
  def self._logger # :nodoc:
    @logger || raise("Missing Object.logger")
  end 

  def self.log_message(data)
    block_given? ? yield : data || 'No log message provided!'
  end


  # ##############################################
  #   Class side logging
  # ##############################################

  def self.log_debug?; self.logger.debug?; end
  def self.log_info?; self.logger.info?; end
  def self.log_deprecated?; self.logger.deprecated?; end
  def self.log_warn?; self.logger.warn?; end
  def self.log_error?; self.logger.error?; end
  def self.log_fatal?; self.logger.fatal?; end

  [:debug, :info, :deprecated, :warn, :error, :fatal].each do |meth|
    question = "#{meth}?".to_sym
    define_singleton_method(question) do
      self.logger.send(question)
    end
    define_singleton_method(meth) do |data = nil|
      if block_given?
        self.logger.send(meth, data, &Proc.new)
      else
        self.logger.send(meth, data)
      end
    end
  end

  def self.debug!
    if block_given?
      logger.debug!(&Proc.new)
    else
      logger.debug!
    end
  end
  #  =begin
  #  # May confict with other functionality?
  #  def self.info!(&proc); logger.info!(&proc); end
  #  def self.warn!(&proc); logger.warn!(&proc); end
  #  def self.error!(&proc); logger.error!(&proc); end
  #  def self.fatal!(&proc); logger.fatal!(&proc); end
  #  =end

  # def self.debug(data=nil)
  #   if block_given?
  #     self.logger.debug(data, &Proc.new)
  #   else
  #     self.logger.debug(data)
  #   end
  # end

  # def self.info(data=nil)
  #   if block_given?
  #     self.logger.info(data, &Proc.new)
  #   else
  #     self.logger.info(data)
  #   end
  # end

  # Normally called without argument or block: computes calling method name.
  def self.deprecated(data=nil)
    if data || block_given?
      if block_given?
        return self.logger.deprecated(data, &Proc.new)
      else
        return self.logger.deprecated(data)
      end
    end
    self.logger.deprecated { caller(5)[0] }
  end

  if RUBY_VERSION >= "2.5"
    def self.warn(data=nil, uplevel: nil)
      if block_given?
        self.logger.warn(data, Proc.new)
      else
        self.logger.warn(data)
      end
    end
  end

  # def self.error(data=nil)
  #   if block_given?
  #     self.logger.error(data, &Proc.new)
  #   else
  #     self.logger.error(data)
  #   end
  # end

  # Unlike Logger#fatal, raises an exception
  def self.fatal(data=nil)
    return unless logger.fatal?
    data = yield if block_given?
    return unless data
    self.logger.fatal(data) 
    STDOUT.flush
    raise( LoggedFatal, log_message(data)) if Logger::RAISE_FATAL_EXCEPTIONS
  end

  # Similar to #fatal, except:
  # * Requires a block, never takes a String
  # * If block returns nil or false, does not log anything or raise an exception.
  # * The block is always evaluated if Logger::PERFORM_SANITY_CHECKS is true
  #
  # Use #sanity_check for unlikely problems you intend not to check at runtime
  # Use #fatal for for problems that should be checked at runtime.
  #
  # Calls #fatal if both:
  # * Logger::PERFORM_SANITY_CHECKS is true
  # * The block argument returns an error message
  # Takes a no-arg block that:
  # * Returns an error message (or some Object whose print statement
  #           makes the error clear) if the sanity check failed
  # * Returns nil or false if the everything is ok
  # Examples:
  # sanity_check { "Unexpected result" if actualResult!=expectedResult }
  # sanity_check { "Rubbish must be nil" if rubbish }
  # sanity_check { "Value must not be nil" if value.nil? }
  # sanity_check { "The sky is falling!" }   # Executed every time.
  # Alternative: assertions in test/unit
  def self.sanity_check
    if Logger::PERFORM_SANITY_CHECKS && (msg = yield)
      fatal('Sanity check failure: ' + msg.to_s )
    end
  end

  # ##############################################
  #   Instance side logging
  # ##############################################

  def log_debug?; self.logger.debug?; end
  def log_info?; self.logger.info?; end
  def log_deprecated?; self.logger.deprecated?; end
  def log_warn?; self.logger.warn?; end
  def log_error?; self.logger.error?; end
  def log_fatal?; self.logger.fatal?; end

  def logger
    self.class.logger
  end
  
  [:debug, :info, :warn, :error].each do |meth|
    define_method(meth) do |data = nil|
      if block_given?
        logger.send(meth, data, &Proc.new)
      else
        logger.send(meth, data)
      end      
    end
  end
  
  if RUBY_VERSION >= "2.5"
    def warn(data=nil, uplevel: nil)
      if block_given?
        self.logger.warn(data, Proc.new)
      else
        self.logger.warn(data)
      end
    end
  end

  # def debug(data=nil)
  #   if block_given?
  #     logger.debug(data, &Proc.new)
  #   else
  #     logger.debug(data)
  #   end
  # end
  #
  # def info(data=nil, &block)
  #   logger.info(data, &block)
  # end

  # Normally called without argument or block: computes calling method name.
  def deprecated(data=nil)
    if data && block_given?
      if block_given?
        return logger.deprecated(data, &Proc.new)
      else
        return logger.deprecated(data)
      end
    end
    logger.deprecated { caller(5)[0] }
  end

  # def warn(data=nil, &block)
  #   logger.warn(data, &block)
  # end
  #
  # def error(data=nil, &block)
  #   logger.error(data, &block)
  # end

  # Unlike Logger#fatal, raises an exception.
  def fatal(data=nil)
    return unless logger.fatal?
    data = yield if block_given?
    return unless data
    logger.fatal(data)
    STDOUT.flush
    raise( LoggedFatal, self.class.log_message(data)) if Logger::RAISE_FATAL_EXCEPTIONS
  end
  
  # See lengthy comment on class-side message
  def sanity_check(&block)
    self.class.sanity_check(&block)
  end
end # Object