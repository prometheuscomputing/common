# must not have the word m-o-d-u-l-e above the next line (so that a Regexp can figure out the m-o-d-u-l-e name)
module Common
  
  # For more information about meta_info.rb, please see project MM, lib/mm/meta_info.rb
  
  GEM_NAME = "common"
  VERSION = '1.12.0'
  AUTHORS = ["Art Griesser"]
  EMAILS = ["a.griesser@prometheuscomputing.com", 'm.faughn@prometheuscomputing.com']
  HOMEPAGE = nil
  SUMMARY = %q{A few utilities shared by many projects}
  DESCRIPTION = %q{Common contains a few utilities. }
  
 
  LANGUAGE = :ruby
  LANGUAGE_VERSION = ['>= 2.3', '< 3']
  RUNTIME_VERSIONS = {
    :mri => ['>= 2.3', '< 3'],
    :jruby => ['~> 9.0']
  }
  TYPE = :utility
  
  DEPENDENCIES_RUBY = { }
  DEPENDENCIES_MRI = { }
  DEPENDENCIES_JRUBY = { }
  DEVELOPMENT_DEPENDENCIES_RUBY = { }
  DEVELOPMENT_DEPENDENCIES_MRI = { }
  DEVELOPMENT_DEPENDENCIES_JRUBY = { }
  
end