class Module
  def attr_alias(new_getter, getter)
    alias_method(new_getter, getter) if method_defined? getter
    setter = "#{getter}="
    new_setter = "#{new_getter}="
    alias_method(new_setter, setter) if method_defined? setter
  end
  
  # Just interesting and possibly useful
  def arity_of_public_instance_methods
    answer = []
    ancestors = self.ancestors
    (self.public_instance_methods).each do |meth|
      m = nil
      owner = nil
      ancestors.each do |ancestor|
        begin
          m = ancestor.method(meth)
          owner = ancestor
        rescue
        end
        break if m
      end
      if m
        a = m.arity
        answer << "#{owner}.#{meth} : #{a}"
      else
        answer << "Could not find an owner for #{meth}"
      end
    end
    answer
  end

  # Just interesting and possibly useful.  Note that #public_instance_methods is NOT a subset of #methods.
  def arity_of_methods
    answer = []
    ancestors = self.ancestors
    (self.methods).each do |meth|
      m = nil
      owner = nil
      ancestors.each do |ancestor|
        begin
          m = ancestor.method(meth)
          owner = ancestor
        rescue
        end
        break if m
      end
      if m
        a = m.arity
        answer << "#{owner}.#{meth} : #{a}"
      else
        answer << "Could not find an owner for #{meth}"
      end
    end
    answer
  end
end
