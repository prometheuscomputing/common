# an IO that allows output to multiple internal IOs
class MultiIO
  def initialize(*targets)
     @targets = targets
  end
  
  # Pass through method calls to target IOs.
  # The return value is from the last target. Other return values are ignored.
  def method_missing(method, *args, &block)
    return_value = nil
    @targets.each{|t| return_value = t.send(method, *args, &block)}
    return_value
  end
  
  # Override respond_to? to return true if all targets respond to the passed method
  def respond_to?(method)
    @targets.all? {|t| t.respond_to?(method)}
  end
end