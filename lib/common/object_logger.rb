require 'logger'
require 'common/constants'

# Object logging.  Lets you assign loggers to Classes or Modules.
# By assigning a logger to Object, all classes/modules will have access to that log,
# unless a more specific log is available.
# Also extends Logger to add shortcut methods to set log level (debug!, info!, warn!, error!, fatal!)
#
# This project differs from common/log because it only defines one additional method to Class and Object (#object_logger)
# It also does not modify Logger in any serious way, making it compatible with any project that needs to make direct use
# of Logger. It also does not need to directly modify String of NilClass. It is more limited in scope and does not attempt
# to define extra log levels or inject behavior into the fatal log level. It does not change the default log level.
# Also, the object logger defaults to using the container's logger (with Object.object_logger as the root) so specifying 
# use_container_logger is not necessary. If it is desired to use a class's superclass logger instead, use_superclass_logger! can be called.
#
# Suggested sematics for logging levels:
#  * debug   Information useful for heavy-duty debugging.
#            The results are thought to be fine.
#            Should NOT be routinely checked in production code.
#  * info    Information useful for getting oriented with the code, but useless for normal operation.
#            The results are thought to be fine.
#            Should NOT be routinely checked in production code.
#  * warn    The user should be advised of something (such as an unimplemented feature).
#            The results may or may not be meaningful: this usually can not be programatically determined, the user must decide.
#            Should be routinely checked in production code.
#  * error   We encountered an error severe enough to render final results meaningless, although we can continue for the time being.
#            This allows us to accumulate multiple errors, so they can all be fixed in one swell foop.
#            Should be routinely checked in production code.
#  * fatal   The error is so severe that we can't proceed any further.
#            Should be routinely checked in production code.
#  
# Usage:
# object_logger Logger.new('project_log')
# object_logger.warn!
#  
# module Foo
#   class Bar
#   end
#   class Baz
#     object_logger Logger.new('baz_log')
#   end
#   class Wombat < Baz
#     # Classes use the container's logger (Foo in this case) unless use_superclass_logger! is specified
#     use_superclass_logger!
#   end
# end
#
# Foo.warn "Issue with Foo!"                   # Logged in 'project_log'
# Foo::Bar.new.warn "Issue with Bar instance!" # Logged in 'project_log'
# Foo::Baz.new.warn "Issue with Baz instance!" # Logged in 'baz_log'
# Foo::Wombat.new.warn "Wombat!"               # Logged in 'baz_log'
# # A block will let you bypass tests and message construction if the level is turned off:
# Foo::Bar.new.warn {"Calamity approaching!" if expensive_check_for_calamity}
#
# Author: S. Dana

# These methods are mixed into Module and Object to provide Class and instance level logging methods
# Methods are prefixed with 'log_' to avoid conflicts with other libraries.
# Require common/object_logger_short to use a similar module with shorter method names.
module ObjectLoggerMethods
  [:debug,  :info,  :warn,  :error,  :fatal].each do |meth|
    define_method("log_#{meth}".to_sym) do |data = nil|
      if block_given?
        object_logger.send(meth, data, &Proc.new)
      else
        object_logger.send(meth, data)
      end
    end
  end

  # def log_debug(data=nil, &block); object_logger.debug(data, &block); end
  # def log_info(data=nil, &block); object_logger.info(data, &block); end
  # def log_warn(data=nil, &block); object_logger.warn(data, &block); end
  # def log_error(data=nil, &block); object_logger.error(data, &block); end
  # def log_fatal(data=nil, &block); object_logger.fatal(data, &block); end
end

class Module
  def object_logger(new_logger = nil)
    @object_logger = new_logger if new_logger
    return @object_logger if @object_logger
    # Container loggers take priority over superclass loggers
    # This can be overridden by calling use_superclass_logger!
    
    # There used to be a method in ruby_extensions.rb called #container that did exactly what the next line does (verbatim copy).  That method was replaced by #namespace, which should do the exact same thing as well.  For some reason it was causing a problem here.  Hopefully this fixes it.
    c = self.name.split('::')[0..-2].join('::').to_const
    return c.object_logger if c && !@use_superclass_logger && self != Object
    return superclass.object_logger if self.is_a?(Class) && superclass && self != Object
    raise "No object logger available. Did you call Object.object_logger(Logger.new(...)) ?"
  end
  
  # Use superclass logger instead of container logger.
  def use_superclass_logger!; @use_superclass_logger = true; end
  
  # Class Logger methods
  include ObjectLoggerMethods
end

class Object
  def object_logger(new_logger = nil)
    self.class.object_logger(new_logger)
  end
  
  # Instance Logger methods
  include ObjectLoggerMethods
end

# Add log level convenience methods to Logger
class Logger
  def log_debug!; self.level = Logger::DEBUG; end
  def log_info!; self.level = Logger::INFO; end
  def log_warn!; self.level = Logger::WARN; end
  def log_error!; self.level = Logger::ERROR; end
  def log_fatal!; self.level = Logger::FATAL; end
end