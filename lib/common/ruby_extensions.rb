# this requires 'send_chained' in order to preserve backwards compatibility.
Kernel.warn 'Requiring common/ruby_extensions is deprecated.  Instead, you should require common/send_chained.rb'
require_relative 'send_chained'
