class Object
  # Allows a 'complex' method to be sent to an object
  # An example of a 'complex' method is: 'size.to_s'
  def send_chained(complex_method, options = {})
    return nil if self.nil?
    default_options = {:ignore_nils => false}
    options = default_options.merge options
    parameters = options[:parameters] || []
    raise "Attempted to send nil method name to #{self.inspect} with parameters: #{parameters.inspect}" unless !complex_method.nil?
    raise "Attempted to send empty method name to #{self.inspect} with parameters: #{parameters.inspect}" unless !complex_method.empty?
    # puts "Sending #{+complex_method+} to #{self.inspect} with parameters: #{parameters.inspect}"
    objects = [self]
    complex_method = complex_method.to_s
    method_parts = complex_method.split('.')
    last_method_part = method_parts.pop
    method_parts.shift if method_parts.first == 'self'
  
    method_parts.each do |method_part|
      method_part, parameters_for_part = MethodParsingHelpers.parse_method_name_into_name_and_params(method_part)
      objects = objects.collect{|obj| obj.send(method_part, *parameters_for_part)}.flatten
      objects.compact! if options[:ignore_nils]
      return nil if objects.include?(nil)
    end  
  
    # Handle any explicit parameters (for things like:  [](:foo)  or  []=('bar')  )
    last_method_part, parameters_for_part = MethodParsingHelpers.parse_method_name_into_name_and_params(last_method_part)
    parameters = parameters_for_part + parameters
    return nil unless objects.any?
    if objects.count < 2
      # necessary to maintain backwards compatibility
      answer = objects.first.send(last_method_part, *parameters)
    else
      answer = objects.collect{|obj| obj.send(last_method_part, *parameters)}.flatten.compact
      return nil if answer.empty?
    end
    answer
  end
  
  # Test to see if an object will respond to a 'complex' method
  # Returns true if a nil object is encountered while traveling the method
  # Ex. of a 'complex' method 'size.to_s'
  def respond_to_chained? complex_method
    obj = self
    complex_method = complex_method.to_s
    method_parts = complex_method.split('.')
    return false unless method_parts.any?
    last_method_part = method_parts.pop
    
    method_parts.each do |method_part|
      meth_params = []
      if params_match = method_part.match(/\((.*)\)/)
        method_part = method_part.split('(').first
        meth_params = params_match[1].split(', ')
        meth_params = meth_params.collect do |param|
          # Make a symbol if it starts with colon
          param = param[1..-1].to_sym if param[0] == ':'
          # Could do other processing here
        end        
      end
      unless obj.respond_to? method_part
        return false
      end
      obj = obj.send(method_part, *meth_params) unless obj == nil
      
      return false if obj.nil?
    end
    
    if params_match = last_method_part.match(/\((.*)\)/)
      last_method_part = last_method_part.split('(').first
    end
    return obj.respond_to?(last_method_part)
  end
end

module MethodParsingHelpers
  def self.parse_method_name_into_name_and_params(original_method_name)
    params_match = original_method_name.match(/(.*)\((.*)\)(.*)/)
    if params_match
      # Remove the parentheses and all in between them to get method_name
      method_name = params_match[1] + params_match[3]
      
      # Split the params up and add to array
      meth_params = params_match[2].split(', ')
      meth_params = meth_params.collect do |param|
        case param[0]
        when ':' # Make a symbol if it starts with colon
          param[1..-1].to_sym if param[0] == ':'
        when '\'', '"'# Strip out quotes if quoted string
          param[1..-2]
        else # Assume to be integer
          param.to_i
        end
      end
      [method_name, meth_params]
    else
      [original_method_name, []]
    end
  end
end
