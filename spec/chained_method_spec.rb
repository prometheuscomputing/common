# Chained method tests
require 'spec_helper'
require 'common/send_chained' # This really includes chained methods code

describe 'Calling chained methods on Objects' do
  it 'should allow calling chained methods' do
    'foo'.send_chained('length').should == 3
    'foo'.send_chained('length.succ.to_f').should == 4.0
  end
  
  it 'should allow calling chained methods with parameters' do
    'foo'.send_chained('+("o").length.to_f').should == 4.0
    'foo'.send_chained('length.+(1).to_f').should == 4.0
    'foo'.send_chained('length.to_f.+(1)').should == 4.0
    # This doesn't work because send chained splits on all '.'s
    # 'foo'.send_chained('length.to_f.+(1.0)').should == 4.0
  end
  
  it 'should be able to tell whether an object responds to a chained method' do
    'foo'.respond_to_chained?('length').should == true
    'foo'.respond_to_chained?('badlength').should == false
    'foo'.respond_to_chained?('length.succ.to_f').should == true
    'foo'.respond_to_chained?('length.succ.to_fail').should == false
  end
  
  it 'should be able to tell whether an object responds to a chained method with parameters' do
    pending "This doesn't work correctly"
    'foo'.respond_to_chained?('+("o")').should == true
    'foo'.respond_to_chained?('badwolf("dr")').should == false
    'foo'.respond_to_chained?('length.+(1).to_f').should == true
    'foo'.respond_to_chained?('length.+(1).to_fail').should == false
    'foo'.respond_to_chained?('length.to_f.+(1)').should == true
  end
end
