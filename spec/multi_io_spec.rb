# Object Logger tests.
require 'spec_helper'
require 'stringio'
require 'common/multi_io'

describe "MultiIO" do
  before(:each) do
    @a = ''
    @io_a = StringIO.new(@a)
    
    @b = ''
    @io_b = StringIO.new(@b)
  end
  
  it "should be able to write to multiple IO objects" do
    multi_io = MultiIO.new(@io_a, @io_b)
    
    multi_io.write 'hello'
    
    @a.should == 'hello'
    @b.should == 'hello'
  end
  
  it "should respond to methods that its targets respond to" do
    multi_io = MultiIO.new(@io_a, @io_b)
    
    multi_io.should respond_to(:puts)
  end
end