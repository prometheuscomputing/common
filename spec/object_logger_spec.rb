# Object Logger tests.
require 'spec_helper'
require 'stringio'
require 'common/object_logger'
require 'common/multi_log_device'

$root_log = ''
$habitat_log = ''
$satellite_log = ''
$deep_space_log = ''

object_logger Logger.new(StringIO.new($root_log))
module NASA
  class Habitat
    object_logger Logger.new(StringIO.new($habitat_log))
  end
end
module Space
  class Junk
  end
  class Satellite
    object_logger Logger.new(StringIO.new($satellite_log))
  end
  class Probe < Satellite
    use_superclass_logger!
  end
  class Capsule < NASA::Habitat
    use_superclass_logger!
  end
  class Station < NASA::Habitat
    attr_accessor :oxygen_checked
    def initialize; @oxygen_checked = false; end
    def check_oxygen_level
      @oxygen_checked = true
      '40'
    end
  end
  module DeepSpace
    object_logger Logger.new(StringIO.new($deep_space_log))
    class DSProbe
    end
    class Telescope
      #ds_log = Logger.new(StringIO.new($deep_space_log))
      #sat_log = Logger.new(StringIO.new($satellite_log))
      ds_io = StringIO.new($deep_space_log)
      sat_io = StringIO.new($satellite_log)
      #object_logger MultiLogDevice.new(ds_log, sat_log)
      object_logger Logger.new(MultiLogDevice.new(ds_io, sat_io))
    end
  end
end

describe "Object Logger" do
  before(:each) do
    $root_log.clear
    $habitat_log.clear
    $satellite_log.clear
    $deep_space_log.clear
  end
  
  # NOTE: this test will fail if common/log is required because it changes the default log level
  #       Commenting out for now.
  # it "should have the default logger level of DEBUG unless otherwise defined" do
  #   Object.object_logger.level.should == Logger::DEBUG
  # end
  
  it "should be able to log instance level logs" do
    s = Space::Station.new
    s.log_warn "Oxygen leak!"
    log_lines = $root_log.split("\n")
    log_lines.length.should == 1
    log_lines.first.should =~ /WARN -- : Oxygen leak!/
  end
  
  it "should be able to log class level logs" do
    s = Space::Station
    s.log_warn "Oxygen leak!"
    $root_log.should =~ /WARN -- : Oxygen leak!/
  end
  
  it "should be able to log to a module log" do
    ds = Space::DeepSpace
    ds.log_warn "Incoming asteroid!"
    $deep_space_log.should =~ /WARN -- : Incoming asteroid!/
    
    dsp = Space::DeepSpace::DSProbe.new
    dsp.log_warn "Exiting solar system!"
    $deep_space_log.split("\n").last.should =~ /WARN -- : Exiting solar system!/
  end
  
  it "should be able to use a more specific logger when specified" do
    s = Space::Satellite.new
    s.log_warn "Space debris!"
    $satellite_log.should =~ /WARN -- : Space debris!/
  end
  
  it "should use the superclass logger if use_superclass_logger! is specified" do
    c = Space::Capsule.new
    c.log_warn "Reentry imminent!"
    $habitat_log.should =~ /WARN -- : Reentry imminent!/
    
    p = Space::Probe.new
    p.log_warn "Solar flare!"
    $satellite_log.should =~ /WARN -- : Solar flare!/
  end
  
  it "should allow log messages in the form of blocks" do
    s = Space::Station.new
    s.object_logger.log_warn!
    # check_oxygen_level should not be called
    s.log_info {"Oxygen level is #{s.check_oxygen_level}"}
    s.oxygen_checked.should be false
    $root_log.should be_empty

    # check_oxygen_level should be called
    s.log_warn {"Oxygen level critical: #{s.check_oxygen_level}!"}
    s.oxygen_checked.should be true
    $root_log.should =~ /WARN -- : Oxygen level critical: 40!/
  end
  
  it "should be able to write to multiple logs using MultiLogDevice" do
    dst = Space::DeepSpace::Telescope.new
    dst.log_warn "Supernova detected!"
    $satellite_log.should =~ /WARN -- : Supernova detected!/
    $deep_space_log.should =~ /WARN -- : Supernova detected!/
  end
end