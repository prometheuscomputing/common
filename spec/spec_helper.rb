require 'rspec'
# Enable both old 'should' syntax and new 'expect' syntax
RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = [:should, :expect]
  end
end

# Load path manipulation to ensure that "require 'common/...'" points to this project and not an installed gem.
$:.unshift(File.dirname(__FILE__) + '/../lib')