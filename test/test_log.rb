#  FIXME redo this as rspec and DO NOT USE treeDump (which has already been removed)
require 'common/log'
require 'common/treeDump'
require 'test/unit'

DEBUG_MESSAGE = 'debug_message'
INFO_MESSAGE = 'info_message'
DEPRECATED_MESSAGE = 'deprecated_message'
WARN_MESSAGE  = 'warn_message'
ERROR_MESSAGE = 'error_message'
FATAL_MESSAGE = 'fatal_message'
AnotherWarnMessage = 'The sky is falling!'


class SomeClass
end

class TestRoot
end

class TestSubclass < TestRoot
  use_superclass_logger
  def gizzard
    deprecated
  end
  def self.wombat
    deprecated
  end
end

module TestModule
  self.logger=Logger.new(Object.logger)
  class TestModuleRoot
    use_container_logger
  end
  class TestModuleSubclass < TestModuleRoot
    use_superclass_logger
  end
  class TestCrossIntoModuleSubclass < TestRoot
    use_container_logger
    def alpha
      beta
    end
    def beta
      gamma
    end
    def gamma
      warn AnotherWarnMessage
    end
  end
end


class TestCrossOutofModuleSubclass < TestModule::TestModuleRoot
end

Object.logger.use_arrayDev

# ====================================================

class Test_Log < Test::Unit::TestCase
  
  def test_default
    inst = Logger.new($stdout)
    assert_equal(Logger::DEFAULT_LEVEL, inst.level)
  end

  def verify_level(logger, is_default_level=true)
    expected = is_default_level ? Logger::DEFAULT_LEVEL : Logger::DEBUG
    assert_equal(expected, logger.level)
    assert logger.fatal?
    assert logger.error?
    assert logger.warn?
    assert logger.deprecated?
    assert is_default_level ^ logger.info?
    assert is_default_level ^ logger.debug?
    obj=Object.new
    assert obj.log_fatal?
    assert obj.log_error?
    assert obj.log_warn?
    assert obj.log_deprecated?
    assert is_default_level ^ obj.log_info?
    assert is_default_level ^ obj.log_debug?
    assert Object.log_fatal?
    assert Object.log_error?
    assert Object.log_warn?
    assert Object.log_deprecated?
    assert is_default_level ^ Object.log_info?
    assert is_default_level ^ Object.log_debug?
  end

  def test_classSide
    assert_equal(Logger::DEFAULT_LEVEL, SomeClass.logger.level)
    verify_level(SomeClass.logger)
    history = do_logging(SomeClass)
    verify_history(history)
  end

  def test_logger_levelChange
    logger = SomeClass.logger
    verify_level(logger)
    logger.level= Logger::DEBUG
    assert_equal(Logger::DEBUG, logger.level)
    verify_level(logger, false)
    history = do_logging(logger, false)
    verify_extra_levels(history)
    logger.level= Logger::DEFAULT_LEVEL
  end

  def test_classSide_levelChange
    SomeClass.logger.level= Logger::DEBUG
    history = do_logging(SomeClass)
    verify_extra_levels(history)
    SomeClass.logger.level= Logger::DEFAULT_LEVEL
  end

  def test_instanceSide_levelChange
    SomeClass.logger.level= Logger::DEBUG
    history = do_logging(SomeClass.new)
    verify_extra_levels(history)
    SomeClass.logger.level= Logger::DEFAULT_LEVEL
  end

  def verify_extra_levels(history)
    assert_equal(6, history.size)
    assert(Regexp.new(DEBUG_MESSAGE) =~ history[0], 'Wrong data at history[0]')
    assert(Regexp.new(INFO_MESSAGE) =~ history[1], 'Wrong data at history[1]')
    assert(Regexp.new(DEPRECATED_MESSAGE) =~ history[2], 'Wrong data at history[2]')
    assert(Regexp.new(WARN_MESSAGE) =~ history[3], 'Wrong data at history[3]')
    assert(Regexp.new(ERROR_MESSAGE) =~ history[4], 'Wrong data at history[4]')
    assert(Regexp.new(FATAL_MESSAGE) =~ history[5], 'Wrong data at history[5]')
  end

  def test_instanceSide
    assert_equal(Logger::DEFAULT_LEVEL, SomeClass.logger.level)
    verify_level(SomeClass.logger)
    history = do_logging(SomeClass.new)
    verify_history(history)
  end

  def test_inheritence
    assert_equal(Logger::DEFAULT_LEVEL, TestModule::TestModuleSubclass.logger.level)
    verify_level(TestModule::TestModuleSubclass.logger)
    history = do_logging(TestModule::TestModuleSubclass.new)
    verify_history(history)
  end

  def test_classSide_santity_check
    history = Object.logger.logdev
    history.clear
    assert_nothing_raised(LoggedFatal) { SomeClass.sanity_check { nil } }
    assert_nothing_raised(LoggedFatal) { SomeClass.sanity_check { false } }
    assert_equal(0, history.size, 'Should be no history entry')
    assert_raise(LoggedFatal) { SomeClass.sanity_check { 'Rubbish'} }
    assert_equal(1, history.size, 'Should be one history entry')
  end

  def test_instanceSide_santity_check
    history = Object.logger.logdev
    history.clear
    assert_nothing_raised(LoggedFatal) { SomeClass.new.sanity_check { nil } }
    assert_nothing_raised(LoggedFatal) { SomeClass.new.sanity_check { false } }
    assert_equal(0, history.size, 'Should be no history entry')
    assert_raise(LoggedFatal) { SomeClass.new.sanity_check { 'Rubbish'} }
    assert_equal(1, history.size, 'Should be one history entry')  
  end

  def test_deprecated
    history = Object.logger.logdev
    history.clear
    TestSubclass.wombat
    where = history[0]
    assert(/DEPRECATED: .* `wombat'/ =~ where, 'Got class method wrong')
    history.clear
    TestSubclass.new.gizzard
    where = history[0]
    assert(/DEPRECATED: .* `gizzard'/ =~ where, 'Got instance method wrong')  
  end

end # Test_Class


def do_logging(logAware, test_exception=true)
 history = Object.logger.logdev
 history.clear
 logAware.debug DEBUG_MESSAGE
 logAware.info  INFO_MESSAGE
 logAware.deprecated  DEPRECATED_MESSAGE
 logAware.warn  WARN_MESSAGE
 logAware.error ERROR_MESSAGE
 assert_raise(LoggedFatal) {logAware.fatal FATAL_MESSAGE} if test_exception
 logAware.fatal FATAL_MESSAGE unless test_exception
 history
end

def verify_history(history)
  assert_equal(4, history.size, 'Should have 4 log entries')
  assert(Regexp.new(DEPRECATED_MESSAGE) =~ history[0], 'Wrong data at history[0]')
  assert(Regexp.new(WARN_MESSAGE) =~ history[1], 'Wrong data at history[1]')
  assert(Regexp.new(ERROR_MESSAGE) =~ history[2], 'Wrong data at history[2]')
  assert(Regexp.new(FATAL_MESSAGE) =~ history[3], 'Wrong data at history[3]')
end

# ====================================================

# if __FILE__ == $0 #directlyInvoked?
#     require 'test/unit/ui/console/testrunner'
#     Test::Unit::UI::Console::TestRunner.run(Test_Log)
# end
